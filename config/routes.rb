Rails.application.routes.draw do

  root to: 'github_users#new'

  get '/users', to: 'github_users#index'
  get '/search', to: 'github_users#show'

  resources :repositories
  resources :github_users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
