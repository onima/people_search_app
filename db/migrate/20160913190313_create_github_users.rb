class CreateGithubUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :github_users do |t|
      t.string :name
      t.integer :stars

      t.timestamps
    end
  end
end
