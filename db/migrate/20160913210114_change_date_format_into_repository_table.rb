class ChangeDateFormatIntoRepositoryTable < ActiveRecord::Migration[5.0]
  def up
    change_column :repositories, :date, :datetime
  end

  def down
    change_column :repositories, :datetime, :date
  end
end
