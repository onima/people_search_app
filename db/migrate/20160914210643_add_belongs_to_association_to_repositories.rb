class AddBelongsToAssociationToRepositories < ActiveRecord::Migration[5.0]
  def change
    change_table :repositories do |t|
      t.belongs_to :github_user, index: true
    end
  end
end
