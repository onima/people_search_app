require 'spec_helper'

describe SearchService do
  let(:search_service) { SearchService.new }
  let(:user_name) { 'onima' }
  let(:repo_1) do
    { name: 'repo_1',
      created_at: Time.new(2015, 10, 31),
      description: 'repo number 1',
      stargazers_count: 4 }
  end
  let(:repo_2) do
    { name: 'repo_2',
      created_at: Time.new(2016, 11, 18),
      description: 'repo number 2',
      stargazers_count: 0 }
  end
  let(:repo_3) do
    { name: 'repo_3',
      created_at: Time.new(2016, 10, 21),
      description: 'repo number 3',
      stargazers_count: 3 }
  end

  before do
    allow(search_service).to receive(:find_user) { { login: user_name }}
    allow(search_service).to receive(:user_repositories) do
      [repo_1, repo_2, repo_3]
    end
  end

  context 'when user search for a github user' do
    describe 'search_repositories' do
      it "returns user' repositories" do
        allow(search_service).to receive(:user_repositories_sorted) do 
          [repo_2, repo_3, repo_1] 
        end

        expect(search_service.search_repositories(user_name)).to match_array(
          [
            ["repo_2", "18 Nov. 2016"],
            ["repo_3", "21 Oct. 2016"],
            ["repo_1", "31 Oct. 2015"] ]
        )
      end
    end

    describe 'search_stars' do
      it "assigns user' stars to @user_stars" do
        allow(search_service).to receive(:user_stars) do
          [4, 0, 3]
        end

        expect(search_service.search_stars(user_name)).to eq(7)
      end
    end
  end
end
