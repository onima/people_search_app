ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)

RSpec.configure do |config|
  config.color = true
  config.formatter = :progress
  config.order = :rand

  config.before(:suite) do
    GithubUser.delete_all
  end

  config.after(:suite) do
    GithubUser.delete_all
  end
end
