require 'spec_helper'

describe GithubUser do
  it "doesn't duplicate github user inside db" do
    GithubUser.create(name: 'onima', stars: 5)
    expect(GithubUser.create(name: 'onima', stars: 7)).not_to be_valid
  end
end
