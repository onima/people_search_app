class SearchService
  def search_repositories(user_name)
    user = find_user(user_name)
    user_repos = user_repositories(user)
    user_repositories_sorted = user_repositories_sorted(user_repos)
    user_repositories_sorted.pluck(:name, :created_at).map do |u_r|
      [ u_r.first, u_r.last.strftime("%d %b. %Y") ]
    end
  end

  def search_stars(user_name)
    user = find_user(user_name)
    user_repos = user_repositories(user)
    user_stars(user_repos).reduce(:+)
  end

  def find_user(user_name)
    Octokit.user(user_name)
  rescue Octokit::NotFound => e
    raise e.message
  end

  def user_repositories(user)
    user.rels[:repos].get.data
  end

  def user_repositories_sorted(user_repos)
    user_repos.sort_by! { |u_r| u_r.created_at }
  end

  def user_stars(user_repos)
    user_repos.map(&:stargazers_count)
  end
end
