class GithubUser < ApplicationRecord
  validates_uniqueness_of :name

  has_many :repositories, dependent: :destroy
end
