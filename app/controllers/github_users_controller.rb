class GithubUsersController < ApplicationController
  SEARCH_SERVICE = SearchService.new

  def index
    @github_users = GithubUser.order(:name)
  end

  def show
    user_name = params[:user_name]
    search_service = SEARCH_SERVICE

    begin
    @user_repositories = search_service.search_repositories(user_name)
    rescue RuntimeError #rescue from find_user method inside services/search_service
      @error = 'User not found'
      return render 'show'
    end

    @user_stars = search_service.search_stars(user_name)
    @user_name = user_name

    unless @user_name == 'Unknown user'
      user_creation(@user_name, @user_stars, @user_repositories)
    end
  end

  def new; end

  def user_creation(user_name, user_stars, user_repositories)
    user_creation = GithubUser.create(name: user_name, stars: user_stars)

    if user_creation.valid?
      user_repositories.each do |u_r|
        user = GithubUser.find_by_name(user_name)
        user.repositories.create!(name: u_r.first, date: u_r.last)
      end
    else
      @error = 'User already exists inside your search database'
    end
  end
end
