## People search app (Code challenge)

### Ruby Version : 2.2.2

### Rails Version : 5.0.0

## To test this application locally

Run ``` bundle ``` ~> install Gems and dependencies

Run ``` rake db:create rake db:migrate ``` ~> create db, run migrations and update schema to the last version

Run ``` rails s ``` ~> Launch server locally : localhost:3000

## To run tests

Running the test suite is simple : ``` rspec ```

## In production

This application has been deployed live with Heroku => [people-search-app](https://people-search-app.herokuapp.com/)
